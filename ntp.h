#ifndef NTP_H
#define NTP_H

#include <time.h>
#include <stdint.h>

#define NTP_TIME_1900_1970  2208988800
#define NTP_SERVER_PORT     123
#define NTP_VERSION         3
#define NTP_MODE            3

#define NTP_REQ_TMO         3
#define NTP_REQ_RETRY       3


#pragma pack(1)
// 1000.2 s
// 1900.1.1 0:00:00  s
typedef struct _ntp_ts_t {
    uint32_t seconds;
    uint32_t fraction;
}ntp_ts_t;

typedef struct _ntp_pkt_t {
    uint8_t LI_VN_Mode;
    uint8_t stratum;
    uint8_t poll;
    uint8_t precision;
    uint32_t root_delay;
    uint32_t root_dispersion;
    uint32_t ref_id;
    ntp_ts_t ref_ts;
    ntp_ts_t orig_ts;
    ntp_ts_t recv_ts;
    ntp_ts_t trans_ts;
}ntp_pkt_t;

#pragma pack()

struct tm * ntp_get_time (const char * server);

#endif
