#include <stdio.h>
#include "ntp.h"

int main (void) {
    const char * server_list[] = {
        "time.windows.com",
    };

    for (int i = 0; i < sizeof(server_list) / sizeof(server_list[0]); i++) {
        struct tm * t = ntp_get_time(server_list[i]);
        if (t) {
            printf("current time is : %s\n", asctime(t));
            return 0;
        }
    }
    printf("ntp: get time failed, stop\n");
    return -1;
}